#pragma once

#include "config.h"

#define JL_XSTR(x) JL_STR(x)
#define JL_STR(x) #x

#define JL_FAIL(s) ((s !!))
/*
* Detect the compiler.
* One of the following macro are defined:
*  JL_COMPILER_GCC
*  JL_COMPILER_CLANG
*  JL_COMPILER_MSVC
*  JL_COMPILER_UNKNOWN
*/
#if defined(__clang__)
#	define JL_COMPILER_CLANG
#   define JL_COMPILER_NAME "clang"
#   define JL_COMPILER_VERSION_STRING __clang_version__
#elif defined(__GNUC__)
#	define JL_COMPILER_GCC
#   define JL_COMPILER_NAME "gcc"
#   define JL_COMPILER_VERSION_STRING __VERSION__
#elif defined(_MSC_VER)
#	define JL_COMPILER_MSVC
#   define JL_COMPILER_NAME "msvc"
#   define JL_COMPILER_VERSION JL_XSTR(_MSC_FULL_VER)
#else 
#	define JL_COMPILER_UNKNOWN
#   define JL_COMPILER_NAME "unknown"
#   define JL_COMPILER_VERSION  "unknown"
#endif

/*
* Detect the platform
* One of the following macro are defined:
* JL_PLATFORM_WIN
* JL_PLATFORM_UNKNOWN
*/
#if defined(_WIN32)
#	define JL_PLATFORM_WIN
#else
#	define JL_PLATFORM_UNKNOWN
#endif

#if defined(JL_PLATFORM_WIN)
#	if defined(JL_COMPILER_MSVC)
#		define JL_SHARED_EXPORT __declspec(dllexport)
#		define JL_SHARED_IMPORT __declspec(dllimport)
#	elif defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#		define JL_SHARED_EXPORT __attribute__ ((dllexport))
#		define JL_SHARED_IMPORT __attribute__ ((dllimport))
#	else
#		define JL_SHARED_EXPORT JL_FAIL(("JL_SHARED_EXPORT is not defined for your compiler"))
#		define JL_SHARED_IMPORT JL_FAIL(("JL_SHARED_IMPORT is not defined for your compiler"))
#	endif
#else
#	define JL_SHARED_EXPORT JL_FAIL(("JL_SHARED_EXPORT is not defined for your platform"))
#	define JL_SHARED_IMPORT JL_FAIL(("JL_SHARED_IMPORT is not defined for your platform"))
#endif

#if defined(JL_SHARED)
#	if defined(JL_BUILD)
#		define JL_API JL_SHARED_EXPORT
#	else
#		define JL_API JL_SHARED_IMPORT
#	endif
#else
#	define JL_API
#endif

#if __cplusplus
#	define JL_C extern "C"
#else
#	define JL_C 
#endif

/*
* Defined macro:

#define jl_unreachable 

Marks an execution path as not reachable, like assert(false).
If control flow ever reaches jl_unreachable, the behavior is undefined.
Compiler should recognize jl_unreachable as no-return, and not produce warnings 
  about the absense of a 'return' instruction.
Usage: 
	if ( a == 3 ){
		do things...
	}else{
		jl_unreachable;
	}



#define jl_debug_trap

Breaks into debugger, if applicable. A dirty way to set a persistent breakpoint/resumable assert.
Usage:
	if ( foo() == 25 ){
		jl_debug_trap();
	}



#define jl_likely(expr)
#define jl_unlikely(expr)

Marks a value as likely/unlikely to be true. Compiler can apply some optimization based on this information.
Should be used to mark rare taken branches in performance critical code.
Usage:
	if ( jl_unlikely(errorCode != 0) ){
		processError();
	}

*/

//Macro hints that the current control flow path is dead and shall never execute
//#define jl_unreachable
#if defined(JL_DEBUG)
#	include "assert.h"
#	if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#		define jl_unreachable (jl_assert(false, "jl_unreachable reached"), __builtin_unreachable())
#	elif defined(JL_COMPILER_MSVC)
#		define jl_unreachable (jl_assert(false, "jl_unreachable reached"), __assume(0))
#	else
#		error Can't define jl_unreachable
#	endif
#else
#	if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#		define jl_unreachable __builtin_unreachable()
#	elif defined(JL_COMPILER_MSVC)
#		define jl_unreachable __assume(0)
#	else
#		error Can't define jl_unreachable
#	endif
#endif

#if defined(JL_DEBUG)
#	if defined(JL_COMPILER_MSVC)
#		define jl_debug_trap __debugbreak()
#	elif defined(JL_PLATFORM_WIN)
#		define jl_debug_trap DebugBreak()
#	else
#		error Can't define jl_debug_trap
#	endif
#else
#	define jl_debug_trap (void)0
#endif



//Macro hints that the value of expr is likely (unlikely) to be true
//#define jl_likely(expr)
//#define jl_unlikely(expr)
#if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#	define jl_unlikely(cond) __builtin_expect((cond), false)
#elif defined(JL_COMPILER_MSVC)
#	define jl_unlikely(cond) (cond)
#else
#	define jl_unlikely(cond) (cond)
#endif

#if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#	define jl_likely(cond) __builtin_expect((cond), true)
#elif defined(JL_COMPILER_MSVC)
#	define jl_likely(cond) (cond)
#else
#	define jl_likely(cond) (cond)
#endif

#if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#	define jl_forceinline __attribute__((always_inline))
#elif defined(JL_COMPILER_MSVC)
#	define jl_forceinline __forceinline
#else
#	warning Can't define jl_forceinline
#endif

#define JL_SYNTHESIZE_ENUM_OPERATOR(enumtype, inttype, op)	\
	inline enumtype operator##op(enumtype lhs, enumtype rhs){ \
		return (enumtype)(((inttype)lhs) op ((inttype)rhs)); \
	} \
	inline enumtype& operator##op##=(enumtype& lhs, enumtype rhs){ \
		lhs = (enumtype)(((inttype)lhs) op ((inttype)rhs)); \
		return lhs; \
	} \

#define JL_SYNTHESIZE_ENUM_BITFIELD(enumtype, inttype) \
	JL_SYNTHESIZE_ENUM_OPERATOR(enumtype, inttype, |) \
	JL_SYNTHESIZE_ENUM_OPERATOR(enumtype, inttype, &) \
	JL_SYNTHESIZE_ENUM_OPERATOR(enumtype, inttype, ^) \
	inline enumtype operator~(enumtype lhs){ \
		return (enumtype)(~((inttype)lhs)); \
	}

#include "platform.h"
