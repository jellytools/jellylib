#pragma once

#include "config.h"
#include "macro.h"

#if defined(JL_PLATFORM_WIN)
#	include "win/platform.h"
#else
#	error Unknown platform
#endif