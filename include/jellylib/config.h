#pragma once

/*
* Jellylib configuration.
* Search '@@' for configuration values
*/

/*
* @@ JL_DEBUG
* @@ JL_RELEASE
* 
* Exactly one of those constants is defined.
* If not given explicitly, JL_RELEASE is set when NDEBUG is set, otherwise, 
* JL_DEBUG is set.
* 
* JL_DEBUG enables extra checks.
*/

#if defined(JL_DEBUG) && defined(JL_RELEASE)
#	error "Both JL_DEBUG and JL_RELEASE are defined"
#elif (!defined(JL_DEBUG)) && (!defined(JL_RELEASE))
#	if defined(NDEBUG)
#		define JL_RELEASE
#	else
#		define JL_DEBUG
#	endif
#endif

/*
* @@ JL_ASSERTS_ENABLED 
*
* Controls whenever asserts are enabled.
* By default, it is set if JL_DEBUG is set.
* You can override the default behavior by setting
* -DJL_ASSERTS_ENABLED=1 or -DJL_ASSERTS_ENABLED=0
*
* If you want to use this constant, however, note that it won't retain 
* its numeric value, so you should test it with defined(JL_ASSERTS_ENABLED)
*/
#if defined(JL_ASSERTS_ENABLED)
#	if JL_ASSERTS_ENABLED
#		undef JL_ASSERTS_ENABLED
#		define JL_ASSERTS_ENABLED
#	else
#		undef JL_ASSERTS_ENABLED
#	endif
#else
#	if defined(JL_DEBUG)
#		define JL_ASSERTS_ENABLED
#	else
#	endif
#endif

/*
* @@ JL_SHARED
*
* Define if jellylib is used as a shared library (both for compiling jellylib
* and its user).
*/


#define JL_TRACE_ALLOCATIONS