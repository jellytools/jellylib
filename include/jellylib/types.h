#pragma once
#pragma once

#include "stdbits.h"

namespace jly {

// I hate typing a lot for really basic types, and I hate _t.
// So typedef common integral types.
using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
 
using uz = size_t;
using iz = intptr_t;

// Rules regarding void* pointers:
//  - Use void* to break the type system rules, to cast something to void*
//    and later cast back
//  - Do not use void* for memory operations, use u8* instead

using cstring = char const*;
using mut_cstring = char*;

// I make the following assumptions about floating point types.
// Its better to fail earlier than to catch obscure bugs later.
static_assert(
	std::numeric_limits<float>::is_iec559,
	"float type is not ieee compatible"
);
static_assert(
	std::numeric_limits<double>::is_iec559,
	"double type is not ieee compatible"
);
static_assert(
	std::numeric_limits<float>::digits == 24,
	"float type is not f32"
);
static_assert(
	std::numeric_limits<double>::digits == 53,
	"double type is not f64"
);
using f32 = float;
using f64 = double;

template<class T>
using own = std::unique_ptr<T>;

}