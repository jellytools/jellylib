#pragma once
#include <jellylib/memory/MemPool.h>
#include <jellylib/macro.h>

#include "StringRef.h"

namespace jly {

	class JL_API Twine {
	public:
		Twine()
			: kind(Kind::Empty) {
		}

		Twine(StringRef str)
			: kind(Kind::Leaf)
			, leaf(str) {
		}

		Twine(const Twine& left, const Twine& right)
			: kind(Kind::NodeNN)
			, nodeNN{ &left, &right } {
		}

		Twine(const StringRef& left, const Twine& right)
			: kind(Kind::NodeLN)
			, nodeLN{ left, &right } {
		}

		Twine(const Twine& left, const StringRef& right)
			: kind(Kind::NodeNL)
			, nodeNL{ &left, right } {
		}

		template<class AllocatorBase>
		StringRef toString(AllocatorBase&& alloc) const {
			size_t n = calculateSize();
			u8* buffer = alloc.allocate(n);
			printToBuffer(buffer);
			return StringRef(buffer, n);
		}

		std::string toString() const {
			std::string str;
			str.resize(calculateSize());
			printToBuffer((u8*)(str.data()));
			return str;
		}

		void appendToString(std::string& str) const {
			size_t baseSize = str.size();
			str.resize(baseSize + calculateSize());
			printToBuffer((u8*)(str.data() + baseSize));
		}

		size_t calculateSize() const { return calculateSize(0); }

		u8* printToBuffer(u8* buffer) const {
			switch (kind) {
			case Kind::Empty: {
				//do nothing
				return buffer;
			}break;
			case Kind::Leaf: {
				size_t n = leaf.getSize();
				memcpy(buffer, leaf.getData(), n);
				return buffer + n;
			}break;
			case Kind::NodeLN: {
				size_t n = nodeLN.lhs.getSize();
				memcpy(buffer, nodeLN.lhs.getData(), n);
				return nodeLN.rhs->printToBuffer(buffer + n);
			}break;
			case Kind::NodeNL: {
				buffer = nodeNL.lhs->printToBuffer(buffer);
				size_t n = nodeNL.rhs.getSize();
				memcpy(buffer, nodeNL.rhs.getData(), n);
				return buffer + n;
			}break;
			case Kind::NodeNN: {
				buffer = nodeNN.lhs->printToBuffer(buffer);
				return nodeNN.rhs->printToBuffer(buffer);
			}break;
			default: jl_unreachable;
			}
		}
	private:
		size_t calculateSize(size_t acc) const {
			switch (kind) {
			case Kind::Empty: return acc;
			case Kind::Leaf: return acc + leaf.getSize();
			case Kind::NodeLN: return nodeLN.rhs->calculateSize(acc + nodeLN.lhs.getSize());
			case Kind::NodeNL: return nodeNL.lhs->calculateSize(acc + nodeNL.rhs.getSize());
			case Kind::NodeNN: return nodeNN.rhs->calculateSize(nodeNN.lhs->calculateSize(acc));
			default: jl_unreachable;
			}
		}

		enum class Kind {
			Empty,
			Leaf,
			NodeNN,
			NodeNL,
			NodeLN
		} kind;

		union {
			StringRef leaf;

			struct {
				const Twine* lhs;
				const Twine* rhs;
			} nodeNN;

			struct {
				StringRef lhs;
				const Twine* rhs;
			} nodeLN;

			struct {
				const Twine* lhs;
				StringRef rhs;
			} nodeNL;
		};
	};

	Twine operator+ (const Twine& lhs, const StringRef& rhs) {
		return Twine(lhs, rhs);
	}

	Twine operator+ (const StringRef& lhs, const Twine& rhs) {
		return Twine(lhs, rhs);
	}

	Twine operator+ (const Twine& lhs, const Twine& rhs) {
		return Twine(lhs, rhs);
	}

}