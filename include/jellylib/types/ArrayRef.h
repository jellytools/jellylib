#pragma once
#include <jellylib/common.h>

namespace jly {

template<class T>
class ArrayRef{
public:
	using iterator = const T *;
	using const_iterator = const T *;
	using size_type = uz;
	using reverse_iterator = std::reverse_iterator<iterator>;
	
	template<class U>
	friend class ArrayRef;

	ArrayRef(nullptr_t)
		: size(0)
		, ptr(nullptr){
	}
	
	ArrayRef(const T* ptr, uz size)
		: size(size)
		, ptr(ptr){
	}

	ArrayRef(const T* begin, const T* end)
		: ptr(begin)
		, size(end - begin){
	}

	ArrayRef(const std::initializer_list<T>& vals)
		: ptr(vals.begin())
		, size(vals.size()){
	}

	template<class A>
	ArrayRef(const std::vector<T, A>& vec)
		: ptr(vec.data())
		, size(vec.size()){
	}

	template<uz N>
	ArrayRef(const std::array<T, N>& array)
		: ptr(array.data())
		, size(N) {
	}

	template<uz N>
	ArrayRef(const T (&arr) [N])
		: size(N)
		, ptr(arr){
	}

	ArrayRef(const T& val)
		: ptr(&val)
		, size(1){
	}

	template<typename TIn, typename = std::enable_if_t< std::is_convertible_v<TIn*, T*> > >
	ArrayRef(const ArrayRef<TIn>& other)
		: ptr(other.ptr)
		, size(other.size) {
	}

	uz getSize() const{
		return size;
	}

	const T* getData() const{
		return ptr;
	}

	const T& get(uz index) const{
		jl_assert(index < size);
		return ptr[index];
	}

	const T& operator[](uz index) const{
		return get(index);
	}

	iterator begin() const {
		return getData();
	}

	iterator end() const {
		return getData() + getSize();
	}

	ArrayRef<T> slice(uz begin, uz end) const{
		return {ptr + begin, end - begin};
	}

	template<class Alc>
	ArrayRef<T> clone(Alc& alc) const{
		T* buf = (T*)alc.allocateSpace<T>(size);
		for ( uz i = 0; i < size; i++ ){
			buf[i] = ptr[i];
		}
		return {buf, size};
	}
private:
	const T* ptr;
	uz size;
};

}