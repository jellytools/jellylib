#pragma once
#include <jellylib/types/ArrayRef.h>
#include <jellylib/memory/MemPool.h>

namespace jly{

template<class T>
class Cons {
public:
	Cons(MemPool::Slice* slice) :
		slice(slice),
		last(nullptr) {
	}

	template<class... Args>
	T& push(Args&&... args) {
		Node* node = (Node*)slice->allocate(sizeof(Node));
		new(&node->val)T(std::forward<Args>(args)...);
		node->next = last;
		last = node;
		return node->val;
	}

	ArrayRef<T> get(MemPool::Slice* slice) {
		uz size = count();
		T* t = (T*)slice->allocateSpace<T>(size);
		Node* node = last;
		uz idx = size - 1;
		while (node) {
			new(t + idx)T(std::move(node->val));
			idx--;
			node->val.~T();
			node = node->next;
		}
		return { t, size };
	}

	T& getLast(){
		return last->val;
	}

	bool empty(){
		return !last;
	}

	uz count() {
		uz size = 0;
		Node* node = last;
		while (node) {
			size++;
			node = node->next;
		}
		return size;
	}
private:
	

	struct Node {
		T val;
		Node* next;
	};
	Node* last;
	MemPool::Slice* slice;
};

}