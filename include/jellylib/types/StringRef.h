#pragma once

#include <jellylib/macro.h>
#include <jellylib/config.h>
#include <jellylib/types/ArrayRef.h>

namespace jly{

class JL_API StringRef : public ArrayRef<u8>{
public:
	using ArrayRef::ArrayRef;

	explicit StringRef(cstring cstr): 
		ArrayRef((const u8*)cstr, strlen(cstr)){
	}

	//convenience constructor for static strings
	template<uz N>
	StringRef(const char (&static_string) [N]): 
		ArrayRef((const u8*)static_string, N - 1){ //the last byte is '\0'
	}

	template<class T, class A>
	StringRef(const std::basic_string<char, T, A>& str): 
		ArrayRef((const u8*)str.data(), str.size()){
	}

	StringRef(ArrayRef<u8> array):
		ArrayRef(array) {
	}

	operator std::string_view() const{
		return {(const char*)getData(), getSize()};
	}

	friend bool operator==(const StringRef& self, const StringRef& other) noexcept;
	friend bool operator!=(const StringRef& self, const StringRef& other) noexcept;
	friend bool operator<(const StringRef& self, const StringRef& other) noexcept;
	friend bool operator>(const StringRef& self, const StringRef& other) noexcept;
	friend bool operator<=(const StringRef& self, const StringRef& other) noexcept;
	friend bool operator>=(const StringRef& self, const StringRef& other) noexcept;

	std::string toString() const{
		return std::string((const char*)getData(), getSize());
	}

	template<class Alc>
	StringRef clone(Alc& alc) const{
		ArrayRef array = ArrayRef::clone(alc);
		return array;
	}
};


inline bool operator==(const StringRef& self, const StringRef& other) noexcept {
	if (self.getSize() != other.getSize()) {
		return false;
	}
	return !memcmp(self.getData(), other.getData(), self.getSize());
}

inline bool operator!=(const StringRef& self, const StringRef& other) noexcept {
	if (self.getSize() != other.getSize()) {
		return true;
	}
	return memcmp(self.getData(), other.getData(), self.getSize());
}

inline bool operator<(const StringRef& self, const StringRef& other) noexcept {
	uz this_size = self.getSize();
	uz other_size = other.getSize();

	if (this_size < other_size) {
		if (this_size == 0) {
			return true;
		}

		return memcmp(self.getData(), other.getData(), this_size) <= 0;
	} else {
		if (other_size == 0) {
			return false;
		}

		return memcmp(self.getData(), other.getData(), other_size) < 0;
	}
}

inline bool operator>(const StringRef& self, const StringRef& other) noexcept {
	return other < self;
}

inline bool operator<=(const StringRef& self, const StringRef& other) noexcept {
	return !(self > other);
}

inline bool operator>=(const StringRef& self, const StringRef& other) noexcept {
	return !(self < other);
}


inline std::ostream& operator<<(std::ostream& out, const StringRef& str){
	return (out << std::string_view((const char*)str.getData(), str.getSize()));
}

}

namespace std {
template <> struct hash<jly::StringRef> {
	size_t operator()(const jly::StringRef& s) const {
		return hash<std::string_view>()(std::string_view((const char*)s.getData(), s.getSize()));
	}
};
}
