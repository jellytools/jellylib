#pragma once
#include <jellylib/types/StringRef.h>
#include <jellylib/memory/MemPool.h>
#include <jellylib/stdbits.h>
namespace jly{

class StringBuilder{
public:
	StringBuilder(MemPool::Slice* slice):
		slice(slice),
		data(slice)
	{
	}

	void append(char ch) {
		data.push_back(ch);
	}

	void append(const StringRef& str){
		if ( str.getSize() > 0 ){
			uz size = data.size();
			data.resize(size + str.getSize());
			memcpy(data.data() + size, str.getData(), str.getSize());
		}
	}

	void prepend(const StringRef& str) {
		if (str.getSize() > 0) {
			uz size = data.size();
			data.resize(size + str.getSize());
			memmove(data.data() + str.getSize(), data.data(), size);
			memcpy(data.data(), str.getData(), str.getSize());
		}
	}

	StringRef view() {
		return { data.data(), data.size() };
	}

	StringRef get(MemPool::Slice* mem){
		u8* buffer = (u8*)mem->allocateSpace<u8>(data.size());
		memcpy(buffer, data.data(), data.size());
		return {buffer, data.size()};
	}
private:
	MemPool::Slice* slice;
	slice_vector<u8> data;
};

}