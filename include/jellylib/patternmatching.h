#pragma once
#include <utility>

#define match(e) \
	{ \
	using __match_type = std::remove_pointer_t<decltype(e)>; \
	auto __match_value = (e); \
	if ( false ){ 

#define caseof(type, var) \
	} else if ( type::Discriminate(__match_value->tag) ) { \
		auto var = (type*)__match_value; \

#define casedef(var) } else { auto var = __match_value;

#define endmatch }}

#define match2(e1, e2) \
	{ \
	using __match_type = std::remove_pointer_t<decltype(e1)>; \
	auto __match_value_1 = (e1); \
	auto __match_value_2 = (e2); \
	if ( false ){ \

#define caseof2(type1, var1, type2, var2) \
	} else if ( type1::Discriminate(__match_value_1->tag) && type2::Discriminate(__match_value_2->tag) ){ \
		auto var1 = (type1*)__match_value_1; \
		auto var2 = (type2*)__match_value_2;

#define casedef2(var1, var2) } else { \
	auto var1 = __match_value_1; \
	auto var2 = __match_value_2;

#define endmatch2 }}

#define SYNTHESIZE_PM_BASE(name, type) \
	template<type::Tag _tag> \
	struct name : type { \
		template<class... Args> \
		name(Args&&... args) : type(_tag, std::forward<Args>(args)...) {} \
		static bool Discriminate(type::Tag tag){ return tag == _tag; } \
	}

#define SYNTHESIZE_PM_BASE2(name, inherit, base_type) \
	template<base_type::Tag _tag> \
	struct name : inherit { \
		template<class... Args> \
		name(Args&&... args) : inherit(_tag, std::forward<Args>(args)...) {} \
		static bool Discriminate(base_type::Tag tag){ return tag == _tag; } \
	}