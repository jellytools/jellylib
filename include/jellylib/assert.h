#pragma once

#include "config.h"
#include "macro.h"

/*
#define jl_assert(condition)
#define jl_assert(condition, message)

If JL_ASSERTS_ENABLED is defined, checks 'condition'.
	If said condition is false, logs/outputs message (or string representation 
	of a condition) and halts program execution.
Otherwise, acts as a noop. Condition is not executed, so you must not rely on 
side effects from it.

Usage:
 jl_assert(2 * 2 == 4)
or
 jl_assert(2 * 2 == 4, "Sanity check")
*/

typedef void(*jl_assertion_callback)(void*, const char* file, int line, const char* function, const char* message);
typedef struct {
	void* ud;
	jl_assertion_callback func;
} jl_assertion_handler;
JL_C jl_assertion_handler JL_API jl_assertion_handler_set(jl_assertion_handler new_handler);
JL_C jl_assertion_handler JL_API jl_assertion_handler_get();
JL_C void JL_API jl_assertion_failed(const char* file, int line, const char* function, const char* message);

#if defined(JL_ASSERTS_ENABLED)
#	define ZZ_JL_ASSERT_2(condition, message) ( (condition) ? ((void)0) : (jl_assertion_failed(__FILE__, __LINE__, "" __FUNCTION__, (message)), (void)0) )
#else
#	if defined(JL_COMPILER_GCC) || defined(JL_COMPILER_CLANG)
#		define ZZ_JL_ASSERT_2(condition, message) (void)0
#	elif defined(JL_COMPILER_MSVC)
#		define ZZ_JL_ASSERT_2(condition, message) __assume(condition)
#	else
#		define ZZ_JL_ASSERT_2(condition, message) (void)0
#	endif
#endif


#define ZZ_JL_ASSERT_1(condition) ZZ_JL_ASSERT_2(condition, #condition)
#define ZZ_JL_GET_MACRO(_1,_2,NAME,...) NAME


#define ZZ_JL_EXPAND(x) x

#define jl_assert(...) ZZ_JL_EXPAND( ZZ_JL_GET_MACRO(__VA_ARGS__, ZZ_JL_ASSERT_2, ZZ_JL_ASSERT_1)(__VA_ARGS__) )

