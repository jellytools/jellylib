#pragma once
#include <jellylib/types/StringRef.h>
#include <jellylib/types/StringBuilder.h>
#include <jellylib/utils/Writer.h>

namespace jly{

// Decodes a JSON string into a UTF-8 string.
// String should not have enclosing quotes.
// Decoding errors insert one or more 0xFFFD characters.
StringRef DecodeJSONString(MemPool::Slice* mem, MemPool::Slice& tmp, StringRef str, i32* first_error);

// Encodes a UTF-8 string content, without quotes.
// Encoding rules:
//  - the only special escapes are \\, \", \n, \r, \t and \0
//  - always escapes "
//  - every other non-printable character is encoded as \xHH
//  - unicode code points are encoded as a byte sequence (as if the string itself
// is a byte string decodable with UTF-8 decoder)
// The resulting string is always a valid UTF-8 string (not implemented yet).
// Encodes 0xFFFD in case of an error and returns the position of the first error
// or -1 if there is no error
i32 EncodeCLiteral(FormattedWriter* writer, StringRef str);

}