#pragma once

namespace jly {
#define JLY_GENERATE_BIT_ENUM_BINOP(enum, base, op) \
	inline enum operator op(enum lhs, enum rhs){ \
		return (enum)((base)lhs op (base)rhs); \
	} \
	inline enum& operator op##=(enum& lhs, enum rhs){ \
		lhs = (enum)((base)lhs op (base)rhs); \
		return lhs; \
	} 

#define JLY_GENERATE_BIT_ENUM_UNOP(enum, base, op) \
	inline enum operator op(enum lhs){ \
		return (enum)(op (base)lhs); \
	} 


#define JLY_GENERATE_BIT_ENUM(enum, base) \
	JLY_GENERATE_BIT_ENUM_BINOP(enum, base, |) \
	JLY_GENERATE_BIT_ENUM_BINOP(enum, base, &) \
	JLY_GENERATE_BIT_ENUM_BINOP(enum, base, ^) \
	JLY_GENERATE_BIT_ENUM_UNOP(enum, base, ~) 

}
