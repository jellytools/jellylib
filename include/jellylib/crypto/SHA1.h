#pragma once
#include <jellylib/types/StringRef.h>

namespace jly{

struct SHA1Signature{
	u8 sig[40];

	SHA1Signature(StringRef message);

	operator StringRef(){
		return {sig, sizeof(sig)};
	}
};


}