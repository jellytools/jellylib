#pragma once
#include <jellylib/types.h>
#include "Memory.h"
#include <stdexcept>

namespace jly {

class JL_API MemPoolBase {
public:
	MemPoolBase(const Allocator& allocator) 
		: alloc(allocator){
	}

	struct MemChunk {
		MemChunk* prev;
		uz size;

		alignas(std::max_align_t)
			u8 payload[1];
	};
protected:
	u8* largeAlloc(uz size) {
		u8* mem = alloc.allocate(sizeof(MemChunk) + size - 1);
		MemChunk* chunk = new(mem)MemChunk;
		chunk->prev = this->chunk;
		chunk->size = size;
		this->chunk = chunk;
		return chunk->payload;
	}

	void discardTopChunk() {
		MemChunk* chunk = this->chunk;
		this->chunk = this->chunk->prev;
		alloc.release((u8*)chunk, sizeof(MemChunk) + chunk->size - 1);
	}

	Allocator alloc;

	MemChunk* chunk;

	u8* cur;
	uz bytesAvail;
};

class JL_API MemPool : public MemPoolBase, public AllocatorLike<MemPool> {
public:
	static const char Name[];

	static thread_local MemPool* ThreadScratchMemPool;
	static thread_local MemPool* ThreadMemPool;

	class JL_API Slice: public AllocatorLike<Slice> {
	public:
		static const char Name[];

		Slice(nullptr_t):
			ptr(nullptr){
		}

		Slice(MemPool& pool)
			: ptr(&pool)
			, top(pool.chunk)
			, bytesAvail(pool.bytesAvail)
			, cur(pool.cur) {
		}

		Slice(Slice&& slice)
			: ptr(slice.ptr)
			, top(slice.top)
			, bytesAvail(slice.bytesAvail)
			, cur(slice.cur) {
			slice.ptr = nullptr;
		}

		Slice& operator=(Slice&& slice) {
			ptr = slice.ptr;
			top = slice.top;
			bytesAvail = slice.bytesAvail;
			cur = slice.cur;
			slice.ptr = nullptr;
			return *this;
		}

		Slice(const Slice&) = delete;

		~Slice() {
			if (!ptr) {
				return;
			}

			while (ptr->chunk != top) {
				ptr->discardTopChunk();
			}
			ptr->bytesAvail = bytesAvail;
			ptr->cur = cur;
		}

		Slice slice() {
			return ptr->slice();
		}

		u8* allocate(uz size) {
			return ptr->allocate(size);
		}

		void release(u8* ptr, uz size){
		}
	private:
		MemPool * ptr;
		MemChunk* top;
		uz bytesAvail;
		u8* cur;
	};

	static thread_local MemPool::Slice ThreadSlice;

	MemPool(const Allocator& alloc, u8* prealloc, uz prealloc_size, uz largeThreshold, uz chunkSize):
		MemPoolBase(alloc),
		largeThreshold(largeThreshold),
		chunkSize(chunkSize)
	{
		chunk = new(prealloc)MemChunk;
		chunk->prev = nullptr;
		chunk->size = prealloc_size - (sizeof(MemChunk) - 1);

		cur = chunk->payload;
		bytesAvail = prealloc_size;
	}

	static Slice GetStackSlice(){
		return ThreadScratchMemPool->slice();
	}

	static Slice* GetPoolSlice() {
		return &ThreadSlice;
	}

	static void InitForCurrentThread();

	Slice slice() {
		return Slice(*this);
	}

	MemPool(const MemPool&) = delete;

	~MemPool() {
		while (chunk->prev) {
			discardTopChunk();
		}
	}

	void release(u8* ptr, uz size) {
	}

	u8* allocate(uz size) {
		if ( size == 0 ){
			return nullptr;
		}

		uz takenSize = (size + alignof(std::max_align_t) - 1) & ~(alignof(std::max_align_t) - 1);

		if (bytesAvail < takenSize) {
			if (takenSize >= largeThreshold) {
				return largeAlloc(takenSize);
			}

			allocateNewChunk();
		}

		u8* ret = cur;
		cur += takenSize;
		bytesAvail -= takenSize;
		return ret;
	}
private:
	uz largeThreshold, chunkSize;

	void allocateNewChunk() {
		largeAlloc(chunkSize);
		cur = chunk->payload;
		bytesAvail = chunkSize;
	}
		
	
};

template<
	uz PreallocSize = (16 * 1024 * 1024 - (sizeof(MemPoolBase::MemChunk) - 1) - sizeof(MemPool) - (alignof(std::max_align_t) - 1)) / alignof(std::max_align_t) * alignof(std::max_align_t),
	uz LargeThreshold = 128 * 1024,
	uz ChunkSize = ((16 * 1024 * 1024 - (sizeof(MemPoolBase::MemChunk) - 1)) - (alignof(std::max_align_t) - 1)) / alignof(std::max_align_t) * alignof(std::max_align_t)
>
class MemPoolPrealloc: public MemPool{
public:
	MemPoolPrealloc(const Allocator& alloc = CAllocator::Instance):
		MemPool(std::move(alloc), prealloc, sizeof(prealloc), LargeThreshold, ChunkSize)
	{
	}

	alignas(MemChunk)
	u8 prealloc[PreallocSize + sizeof(MemPoolBase::MemChunk) - 1];
};

template<class T>
struct pool_allocator: public std::allocator<T>{
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef pool_allocator<_Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)MemPool::ThreadMemPool->allocateSpace<T>(n);
	}

	void deallocate(pointer p, size_type n) {
	}

	pool_allocator() throw() { }
	pool_allocator(const pool_allocator &a) throw() {}
	template <class U>
	pool_allocator(const pool_allocator<U> &a) throw() {}
	~pool_allocator() throw() {}
private:
	template<class U>
	friend struct pool_allocator;
};


template<class T>
struct slice_allocator : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef slice_allocator<_Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)slice->allocateSpace<T>(n);
	}

	void deallocate(pointer p, size_type n) {
	}

	slice_allocator(MemPool::Slice* slice) throw() : slice(slice) {}
	slice_allocator(const slice_allocator &a) throw() : slice(a.slice) { }
	template <class U>
	slice_allocator(const slice_allocator<U> &a) throw() : slice(a.slice) {}
	~slice_allocator() throw() {}
private:
	template<class U>
	friend struct slice_allocator;

	MemPool::Slice* slice;
};


template<class T>
using pool_vec = std::vector< T, pool_allocator<T> >;

template<class K, class V, class H = std::hash<K>, class Eq = std::equal_to<K> >
using pool_umap = std::unordered_map< K, V, H, Eq, pool_allocator< std::pair<const K, V> > >;

template<class K, class V, class H = std::hash<K>, class Eq = std::equal_to<K> >
using pool_umultimap = std::unordered_multimap< K, V, H, Eq, pool_allocator< std::pair<const K, V> > >;

template<class K, class H = std::hash<K>, class Eq = std::equal_to<K> >
using pool_uset = std::unordered_set< K, H, Eq, pool_allocator< K > >;

template<class V>
using slice_vector = std::vector< V, slice_allocator< V > >;

using slice_string = std::basic_string< char, std::char_traits<char>, slice_allocator< char > >;

template<class K, class V, class Pr = std::less<> >
using slice_map = std::map< K, V, Pr, slice_allocator< std::pair<const K, V> > >;

template<class K, class V, class H = std::hash<K>, class Eq = std::equal_to<K> >
using slice_umap = std::unordered_map< K, V, H, Eq, slice_allocator< std::pair<const K, V> > >;

template<class K, class H = std::hash<K>, class Eq = std::equal_to<K> >
using slice_uset = std::unordered_set< K, H, Eq, slice_allocator< K > >;

template<class K, class V, class Pr = std::less<> >
using slice_multimap = std::multimap< K, V, Pr, slice_allocator< std::pair<const K, V> > >;

template<class K, class V, class Pr = std::less<> >
using slice_umultimap = std::multimap< K, V, Pr, slice_allocator< std::pair<const K, V> > >;


}