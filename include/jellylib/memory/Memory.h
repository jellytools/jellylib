#pragma once
#include <new>
#include <jellylib/types.h>
#include <jellylib/macro.h>



namespace jly{

class AllocationFailedException: public std::runtime_error{
public:
	AllocationFailedException(cstring allocatorName, uz requestSize)
		: runtime_error("allocation request failed")
		, requestSize(requestSize)
		, allocatorName(allocatorName){
	}

	uz getRequestSize() const{
		return requestSize;
	}

	cstring getAllocatorName() const{
		return allocatorName;
	}
private:
	uz requestSize;
	cstring allocatorName;
};


/**
* Allocator concept:
*
*   u8* allocate(uz size);
*   void release(u8* ptr, uz size);
*/

static const size_t RequestedAllocationTooLarge = ~0;

[[noreturn]]
inline void JL_API AllocationFailed(cstring allocatorName, size_t requestSize){
	throw AllocationFailedException(allocatorName, requestSize);
}

template<class Alloc>
class AllocatorLike{
public:
	u8* allocate(uz size) {
		return _allocate(size);
	}

	u8* allocate(uz size, uz count) {
		if ( count != 0 && size * count / count != size ){
			allocationFailed(RequestedAllocationTooLarge);
		}
		return _allocate(size * count);
	}

	template<class T>
	u8* allocateSpace() {
		return allocate(sizeof(T));
	}

	template<class T>
	u8* allocateSpace(uz count) {
		return allocate(sizeof(T), count);
	}

	template<class T>
	void releaseSpace(u8* ptr) {
		release(ptr, sizeof(T));
	}

	template<class T>
	void releaseSpace(u8* ptr, uz count) {
		release(ptr, sizeof(T), count);
	}

	template<class T, class... Args>
	T* create(Args&&... args) {
		return new(allocate(sizeof(T)))T(std::forward<Args>(args)...);
	}

	template<class T, class... Args>
	T* createArray(uz n, Args&&... args) {
		return new(allocate(sizeof(T), n))T[n]{ std::forward<Args>(args)... };
	}

	template<class T>
	void destroy(T* t){
		t->~T();
		_release((u8*)t, sizeof(T));
	}

	template<class T>
	void destroyArray(T* t, uz n) {
		for ( uz i = 0; i < n; i++ ){
			t[i]->~T();
		}
		_release((u8*)t, sizeof(T) * n);
	}

	void release(u8* ptr, uz size, uz count){
		_release(ptr, size * count);
	}
protected:
	void allocationFailed(uz requestSize) {
		AllocationFailed(Alloc::Name, requestSize);
	}
private:
	jl_forceinline 
	u8* _allocate(uz size){
		return ((Alloc*)this)->allocate(size);
	}

	jl_forceinline 
	void _release(u8* ptr, uz size) {
		((Alloc*)this)->release(ptr, size);
	}
};

class JL_API CAllocator: public AllocatorLike<CAllocator>{
public:	
	static const char Name[];
	static CAllocator Instance;

	u8* allocate(uz size){
		u8* ptr = (u8*)malloc(size);
		if ( !ptr ){
			allocationFailed(size);
		}
		return ptr;
	}

	void release(u8* ptr, uz size){
		free(ptr);
	}
};

/* Polymorphic allocator */
class JL_API Allocator : public AllocatorLike<Allocator>{
public:
	static const char Name[];

	Allocator(const Allocator& other):
		base(other.base),
		allocate_fn(other.allocate_fn),
		release_fn(other.release_fn){
	}

	template<class Instance, typename=std::enable_if_t< !std::is_same_v<Instance, Allocator> > >
	Allocator(Instance& inst)
		: base((void*)&inst)
		, allocate_fn(&Allocate<Instance>)
		, release_fn(&Release<Instance>) {
	};

	u8* allocate(uz size) {
		return allocate_fn(base, size);
	}

	void release(u8* ptr, uz size) {
		release_fn(base, ptr, size);
	}
private:
	template<class Instance>
	static u8* Allocate(void* base, uz size){
		Instance* inst = (Instance*)base;
		return inst->allocate(size);
	}

	template<class Instance>
	static void Release(void* base, u8* ptr, uz size) {
		Instance* inst = (Instance*)base;
		inst->release(ptr, size);
	}
	
	u8* (*allocate_fn)(void* base, uz size);
	void (*release_fn)(void* base, u8* ptr, uz size);
	void* base;
};


}