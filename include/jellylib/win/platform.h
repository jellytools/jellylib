#pragma once

#if !defined(NOMINMAX)
#	define NOMINMAX
#endif

#if !defined(WIN32_LEAN_AND_MEAN)
#	define WIN32_LEAN_AND_MEAN
#endif

#if !defined(UNICODE)
#	error Define UNICODE!
#endif

#if !defined(_UNICODE)
#	error Define _UNICODE!
#endif


#include <Windows.h>
#include <winsock2.h>