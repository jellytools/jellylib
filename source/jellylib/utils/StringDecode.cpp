#include <jellylib/utils/StringDecode.h>

namespace jly{

namespace{

const u8 hex_digit[] = "0123456789abcdef";

u32 hexdecode(u8 ch){
	if ( ch >= '0' && ch <= '9' ){
		return ch - '0';
	}
	if (ch >= 'a' && ch <= 'f') {
		return ch - 'a';
	}
	if (ch >= 'A' && ch <= 'F') {
		return ch - 'A';
	}
	return -1;
}

}

StringRef DecodeJSONString(MemPool::Slice* mem, MemPool::Slice& tmp, StringRef str, i32* first_error) {
	#define REPORT if ( first_error ){ *first_error = (i32)i; first_error = nullptr; } continue;
	#define CHECKI if ( i >= n ) { REPORT }
	#define TAKE(var) u8 var; CHECKI; var = str[i]; 
	StringBuilder sb(&tmp);
	uz i = 0;
	uz n = str.getSize();
	for ( ; i < n; i++ ){
		u8 ch = str[i];
		if ( ch == '\\' ){
			i++;
			CHECKI;
			switch ( ch ){
			case '"': sb.append('"'); break;
			case '\\': sb.append('\\'); break;
			case '/': sb.append('/'); break;
			case 'b': sb.append('\b'); break;
			case 'f': sb.append('\f'); break;
			case 'n': sb.append('\n'); break;
			case 'r': sb.append('\r'); break;
			case 't': sb.append('\t'); break;
			case 'x':{
				TAKE(a);
				TAKE(b);
				u32 x = hexdecode(a);
				u32 y = hexdecode(b);
				if ( x < 0 || y < 0 ){
					REPORT;
				}
				sb.append(x * 16 + y);
			}break;
			default:{
				REPORT;
			}break;
			}
		}else{
			sb.append(ch);
		}
	}
	#undef CHECKI
	#undef REPORT
	return sb.get(mem);
}

i32 EncodeCLiteral(FormattedWriter* writer, StringRef str){
	for ( auto ch : str ){
		if ( ch < 128 && isprint(ch) ){
			switch ( ch ){
			case '\n': writer->write("\\n"); break;
			case '\r': writer->write("\\r"); break;
			case '\t': writer->write("\\t"); break;
			case '\\': writer->write("\\\\"); break;
			case '"': writer->write("\\\""); break;
			default: writer->write(ch);
			}
		}else{
			writer->write("\\");
			writer->write(((ch / 64) % 8) + '0');
			writer->write(((ch / 8) % 8) + '0');
			writer->write((ch % 8) + '0');
		}
	}
	return -1;
}


}