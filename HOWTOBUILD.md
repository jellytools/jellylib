# Project Overview

  - `include` directory contains public header files. Expose `include` directory from each subproject via `-I` option.
  This directory contains a separate subdirectory per module, so for `jellylib` module you should look inside the `include/jellylib`.
  
  - `src` directory contains implementation. You should compile each file from the `src` directory. Same as with the `include`, it contains a subdirectory for each module.

  - `vs2017` directory contains Visual Studio specific files and projects.

## Compiling with Multiple Subprojects

To compile a project using other subprojects following the `jellylib` scheme, you need to put them all under the same root directory (we call it *workspace root*):

	workspace/
		jellylib/
			include/...
			src/...
			vs2017/...
			...
		jellyparser/
			include/...
			src/...
			...
		...

Compile each submodule as a library. Link all the libraries into the final executable.
  
## Precompiled Headers

There are `pch.cpp` and `pch.h` in the `src/<module>`. If you do use precompiled headers (you should), force `pch.h` inclusion into each compilation unit, and use `pch.cpp` to produce a precompiled header. 
