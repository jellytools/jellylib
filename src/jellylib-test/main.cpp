#define CATCH_CONFIG_RUNNER
#include <catch/catch.hpp>
#include <jellylib/assert.h>

namespace{

class assertion_failed: public std::exception{
public:
	assertion_failed(const std::string& what)
		: message(what)
		, exception(message.c_str()){
	}
private:
	std::string message;
};

}

int main(int argc, char* argv[]) {
#if defined(_MSC_VER)
	auto at_exit = [](){
		if ( IsDebuggerPresent() ){
			system("pause");
		}
	};
	std::atexit(at_exit);
	std::at_quick_exit(at_exit);
#endif

	jl_assertion_handler_set({nullptr, [](
		void*, 
		const char* file, 
		int line, 
		const char* function, 
		const char* message
	){
		//For testing, override default behavior with an exception
		throw assertion_failed(
			std::string(file) 
			+ " " 
			+ function 
			+ "(" + std::to_string(line) + "): " 
			+ message
		);
	}});

	int result = Catch::Session().run(argc, argv);

	return result;
}