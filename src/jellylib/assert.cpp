#include <jellylib/assert.h>

#if defined(JL_ASSERTS_ENABLED)

static jl_assertion_handler handler = {nullptr, nullptr};

JL_C jl_assertion_handler JL_API 
jl_assertion_handler_set(jl_assertion_handler new_handler){
	jl_assertion_handler old_handler = handler;
	handler = old_handler;
	return old_handler;
}

JL_C jl_assertion_handler JL_API 
jl_assertion_handler_get(){
	return handler;
}

JL_C void JL_API 
jl_assertion_failed(const char* file, int line, const char* function, const char* message){
	if ( !handler.func ){
		fprintf(stderr, "%s %s(%d): %s", file, function, line, message);
		abort();
	}else{
		handler.func(handler.ud, file, line, function, message);	
	}
}

#endif