#include <jellylib/memory/MemPool.h>

namespace jly{

const char MemPool::Name[] = "MemPool";
const char MemPool::Slice::Name[] = "MemPool::Slice";

thread_local MemPool* MemPool::ThreadMemPool = nullptr;
thread_local MemPool* MemPool::ThreadScratchMemPool = nullptr;
thread_local MemPool::Slice MemPool::ThreadSlice(nullptr);

void MemPool::InitForCurrentThread(){
	if ( !ThreadMemPool ){
		ThreadMemPool = CAllocator::Instance.create< MemPoolPrealloc<> >(CAllocator::Instance);
		ThreadSlice = ThreadMemPool->slice();
	}
	if ( !ThreadScratchMemPool ){
		ThreadScratchMemPool = CAllocator::Instance.create< MemPoolPrealloc<> >(CAllocator::Instance);
	}
}

}
